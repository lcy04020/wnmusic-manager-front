import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'backLogin',
    component: () => import('@/views/BackLoginView.vue')
  },
  {
    path: '/systemManager',
    name: 'systemManager',
    redirect: '/home',
    component: () => import('@/views/SystemManagerView.vue'),
    children:[
      {name:'首页',path:'/home',component: () => import('@/views/HomeView.vue')},
      {name:'用户管理',path:'/userManager',component: () => import('@/views/UserManagerView.vue')},
      {name:'歌曲管理',path:'/songManager',component: () => import('@/views/SongManagerView.vue')},
      {name:'歌手管理',path:'/singerManager',component: () => import('@/views/SingerManagerView.vue')},
      {name:'专辑管理',path:'/songAlbumManager',component: () => import('@/views/SongAlbumManagerView.vue')},
      {name:'歌单管理',path:'/songListManager',component: () => import('@/views/SongListManagerView.vue')},
      {name:'评论管理',path:'/commentManager',component: () => import('@/views/CommentManagerView.vue')},
      {name:'评分管理',path:'/songRankManager',component: () => import('@/views/SongRankManagerView.vue')},
	  {name:'角色管理',path:'/roleManager',component: () => import('@/views/RoleManagerView.vue')},
	  {name:'角色管理',path:'/menuManager',component: () => import('@/views/MenuManagerView.vue')},
    ]

  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


export default router
